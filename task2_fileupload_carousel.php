<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="assest/BO/css/bootstrap.min.css">
    <script src="assest/BO/js/jquery.min.js"></script>
    <script src="assest/BO/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Upload ur file</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg bg-warning" data-toggle="modal" data-target="#myModal">click for  Modal</button>

    <!-- Modal -->
    <div class="modal fade " id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please insert the data in form</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" height="500px">
                        <label for="usr">File Descriptions:</label>
                        <input type="text" class="form-control" id="usr" placeholder="please descripe about your file">
                    </div>

                    <div class="date">
                        <label>
                            <input type="date"> select date
                        </label>
                    </div>

                    <div class="form-group" action="carousel.php" method="post" enctype="multipart/form-data" >
                        <label class="control-label">upload file</label>
                        <input type="file" class="filestyle" data-icon="false">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" onclick="location.href='carousel.php'"class="btn btn-default" data-dismiss="modal" name="submit">Send</button>
                </div>

            </div>
        </div>

    </div>

</body>
</html>